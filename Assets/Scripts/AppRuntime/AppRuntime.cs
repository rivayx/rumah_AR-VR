﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class AppRuntime : BaseRuntime
{
    [SerializeField]
    private StateID _FirstState;

    void Awake()
    {
        if (Singleton.Instance == null)
        {
            Singleton.Instance = gameObject.AddComponent<Singleton>();
            DontDestroyOnLoad(gameObject);
        }
    }

    void Start()
    {
        MakeFSM();
    }

    protected override void MakeFSM()
    {
        HomeState homeState = new HomeState();
        homeState.AddTRANSITION(Transition.TRANSITION_TO_KPR, StateID.KPR);
        homeState.AddTRANSITION(Transition.TRANSITION_TO_AR, StateID.AR);
        homeState.AddTRANSITION(Transition.TRANSITION_TO_PROFILE, StateID.PROFILE);
        homeState.AddTRANSITION(Transition.TRANSITION_TO_CONTACT, StateID.CONTACT);

        KPRState kprState = new KPRState();
        kprState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);

        ARState arState = new ARState();
        arState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);
        arState.AddTRANSITION(Transition.TRANSITION_TO_VR, StateID.VR);

        VRState vrState = new VRState();
        vrState.AddTRANSITION(Transition.TRANSITION_TO_AR, StateID.AR);

        ProfileState profileState = new ProfileState();
        profileState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);

        ContactState contactState = new ContactState();
        contactState.AddTRANSITION(Transition.TRANSITION_TO_HOME, StateID.HOME);

        _FSM = new FSMSystem(this);

        switch (_FirstState)
        {
            case StateID.HOME:
                _FSM.AddState(homeState);
                break;
            case StateID.KPR:
                _FSM.AddState(kprState);
                break;
            case StateID.PROFILE:
                _FSM.AddState(profileState);
                break;
            case StateID.CONTACT:
                _FSM.AddState(contactState);
                break;
            case StateID.AR:
                _FSM.AddState(arState);
                break;
            case StateID.VR:
                _FSM.AddState(vrState);
                break;
        }


        _FSM.AddState(homeState);
        _FSM.AddState(kprState);
        _FSM.AddState(arState);
        _FSM.AddState(vrState);
        _FSM.AddState(profileState);
        _FSM.AddState(contactState);

        base.MakeFSM();
    }
}
