﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class ControlCamera : MonoBehaviour
{
    Vector3 FirstPoint;
    Vector3 SecondPoint;
    float xAngle;
    float yAngle;
    float xAngleTemp;
    float yAngleTemp;

    public bool gyroscope { get; set; }
    //public bool move { get; set; }
    public VRRoom room { get; set; }

    void Awake()
    {
        if (!SystemInfo.supportsGyroscope)
        {
            gyroscope = false;
            Debug.LogWarning("WARNING: Gyro not supported");
        }
        else
        {
            gyroscope = true;
        }
    }

    void Start()
    {
        xAngle = 0;
        yAngle = 0;
        transform.rotation = Quaternion.Euler(yAngle, xAngle, 0);
        transform.position = Vector3.zero;

        room = VRRoom.KamarUtama;
    }

    void Update()
    {
        if (gyroscope)
        {
            Input.gyro.enabled = true;

            Quaternion transQuat = new Quaternion(Input.gyro.attitude.x, Input.gyro.attitude.y,
                                                     -Input.gyro.attitude.z, -Input.gyro.attitude.w);
            gameObject.transform.rotation = Quaternion.Euler(90, 0, 0) * transQuat;
        }
        else
        {
#if UNITY_EDITOR
            if (Input.GetMouseButton(0))
            {
                float rotateSpeed = 300f;
                float rotateAboutX = Input.GetAxis("Mouse Y") * Time.deltaTime * rotateSpeed;
                float rotateAboutY = -Input.GetAxis("Mouse X") * Time.deltaTime * rotateSpeed;
                gameObject.transform.Rotate(rotateAboutX, rotateAboutY, 0.0f);

                var newRotation = gameObject.transform.rotation.eulerAngles;
                newRotation.z = 0;
                gameObject.transform.rotation = Quaternion.Euler(newRotation);
            }
#else

            if (Input.touchCount > 0)
            {
                if (Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    FirstPoint = Input.GetTouch(0).position;
                    xAngleTemp = xAngle;
                    yAngleTemp = yAngle;
                }
                if (Input.GetTouch(0).phase == TouchPhase.Moved)
                {
                    SecondPoint = Input.GetTouch(0).position;
                    xAngle = xAngleTemp - (SecondPoint.x - FirstPoint.x) * 180 / Screen.width;
                    yAngle = yAngleTemp + (SecondPoint.y - FirstPoint.y) * 90 / Screen.height;
                    this.transform.rotation = Quaternion.Euler(yAngle, xAngle, 0.0f);
                }
            }
#endif

        }
    }
}
