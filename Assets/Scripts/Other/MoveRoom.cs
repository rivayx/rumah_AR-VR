﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class MoveRoom : MonoBehaviour 
{
    [SerializeField]
    private VRRoom _Room;
    [SerializeField]
    private Transform _TargetRoom;
    [SerializeField]
    private bool doubleClick;
    private Transform _Player;

    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.5f;

    bool move;

    void Start()
    {
        _Player = FindObjectOfType<Camera>().transform;
    }

    void Update () 
    {
        if (move)
        {
            _Player.position = Vector3.Lerp(_Player.position, _TargetRoom.position, Time.deltaTime * 10);
            //_Player.rotation = Quaternion.Lerp(_Player.rotation, Quaternion.Euler(Vector3.zero), Time.deltaTime * 8);
            float dist = Mathf.Abs(_Player.position.x - _TargetRoom.position.x);
            //Debug.Log(dist);
            if (dist < 0.00001f)
            {
                move = false;
            }
        }
	}

    private void OnMouseDown()
    {
        if (doubleClick)
        {
            clicked++;
            if (clicked == 1) clicktime = Time.time;
            if (clicked > 1 && Time.time - clicktime < clickdelay)
            {
                clicked = 0;
                clicktime = 0;
                move = true;
                _Player.GetComponent<ControlCamera>().room = _Room;
            }
            else if (clicked > 2 || Time.time - clicktime > 1) clicked = 0;
        }
        else
        {
            move = true;
            _Player.GetComponent<ControlCamera>().room = _Room;
        }
    }
}
