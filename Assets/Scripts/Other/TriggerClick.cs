﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.Events;

public class TriggerClick : MonoBehaviour
{
    [SerializeField]
    private bool doubleClick;

    public UnityAction TrigerAction { get; set; }

    float clicked = 0;
    float clicktime = 0;
    float clickdelay = 0.5f;

    private void OnMouseDown()
    {
        if (doubleClick)
        {
            clicked++;
            if (clicked == 1) clicktime = Time.time;
            if (clicked > 1 && Time.time - clicktime < clickdelay)
            {
                clicked = 0;
                clicktime = 0;
                TrigerAction();
            }
            else if (clicked > 2 || Time.time - clicktime > 1) clicked = 0;
        }
        else
        {
            Debug.Log("Click");
        }
    }
}
