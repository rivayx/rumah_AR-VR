﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowRP : MonoBehaviour {

    [SerializeField]
    private Text Rp;

	void Update () 
    {
        if (GetComponent<InputField>().text != "")
        {
            Rp.fontStyle = FontStyle.Normal;
            Rp.supportRichText = false;
            Rp.color = GetComponent<InputField>().textComponent.color;
        }
        else
        {
            Rp.fontStyle = FontStyle.Italic;
            Rp.supportRichText = true;
            Rp.color = GetComponent<InputField>().placeholder.color;
        }
	}
}
