﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeInfo : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private bool CanSwipe = false;
    private HomeModal homeModal;

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        homeModal = HomeModal.Instance();
        CanSwipe = true;
    }

    public void OnPointerExit(PointerEventData pointerEventData)
    {
        CanSwipe = false;
    }

    void Update()
    {
        if (CanSwipe)
        {
            if (SwipeManager.IsSwipingLeft())
            {
                homeModal.IncrementInfoPosition();
            }
            if (SwipeManager.IsSwipingRight())
            {
                homeModal.DecrementInfoPosition();
            }
        }
    }
}
