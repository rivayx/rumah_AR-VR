﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;

public class AutoRotate : MonoBehaviour 
{
    [SerializeField]
    private Rotate rotate;

	void Update () 
    {
        switch (rotate)
        {
            case Rotate.x:
                transform.Rotate(Time.fixedDeltaTime * 15, 0, 0);
                break;
            case Rotate.y:
                transform.Rotate(0, Time.fixedDeltaTime * 15, 0);
                break;
            case Rotate.z:
                transform.Rotate(0, 0, Time.fixedDeltaTime * 15);
                break;
        }
	}
}
