﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BaseApps;
using SimpleJSON;
using UnityEngine.EventSystems;

public class Singleton : MonoBehaviour
{
    public StateID ActiveState { get; set; }
    public TypeClass Type { get; set; }

    public EventSystem GetEventSystem { get; set; }

    public static Singleton Instance { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            if (Instance == null)
            {
                Instantiate(Instance);
                Debug.Log("error when trying to create singleton");
            }
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public GameObject GetSelectedUI
    {
        get
        {
            return GetEventSystem.currentSelectedGameObject;
        }

    }
}
