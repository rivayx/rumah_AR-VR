﻿using BaseApps;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemClass
{

}

public class MarketingClass : ItemClass
{
    public string ID { get; set; }
    public string Name { get; set; }
    public string Phone { get; set; }

    public MarketingClass(string id, string name, string phone)
    {
        ID = id;
        Name = name;
        Phone = phone;
    }
}

public class TypeClass : ItemClass
{
    public string ID { get; set; }
    public string Name { get; set; }
    public int Price { get; set; }

    public TypeClass(string id, string name, int price)
    {
        ID = id;
        Name = name;
        Price = price;
    }
}