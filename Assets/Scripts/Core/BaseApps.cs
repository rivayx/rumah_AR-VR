﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace BaseApps
{

    public class JSONEvent : UnityEvent<string>
    {

    }


    public struct CONST_VAR
    {
    }

    public enum Transition
    {
        NullTransition = 0,
        TRANSITION_TO_HOME = 1,
        TRANSITION_TO_KPR = 2,
        TRANSITION_TO_AR = 3,
        TRANSITION_TO_VR = 4,
        TRANSITION_TO_PROFILE = 5,
        TRANSITION_TO_CONTACT = 6,
    }

    public enum StateID
    {
        NullStateID = 0,
        HOME = 1,
        KPR = 2,
        AR = 3,
        VR = 4,
        PROFILE = 5,
        CONTACT = 6,
    }

    public enum Rotate
    {
        NullRotate = 0,
        x = 1,
        y = 2,
        z = 3
    }

    public enum VRRoom
    {
        NullRoom = 0,
        RuangKeluarga = 1,
        KamarUtama = 2,
        KamarMandi = 3,
    }
}
