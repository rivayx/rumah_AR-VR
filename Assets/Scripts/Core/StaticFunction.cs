﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using System.Globalization;
using System;
using System.Text.RegularExpressions;

public class StaticFunction
{
    public static string FormatCurrency(double value)
    {
        return "Rp " + FormatGeneral(value);
    }

    public static string FormatGeneral(double value)
    {
        return FormatNumber("N0", value);
    }

    public static string FormatNumber(string format, double value)
    {
        CultureInfo id = new CultureInfo("id-ID");

        string val = value.ToString(format, id);

        return val;
    }
    
    public static string Whatsapp(string phone)
    {
        return "whatsapp://send?phone=" + phone;
    }
    public static string GoogleMaps(float lat, float lon)
    {
        return "https://maps.google.com/maps?q=" + lat + "," + lon;
    }

    public static string GetFileInStreamingAssets(string file_name)
    {
        string path;
#if UNITY_EDITOR
        path = "file://" + Application.dataPath + "/StreamingAssets/";
#elif UNITY_ANDROID
        path = "jar:file://"+ Application.dataPath + "!/assets/";
        #elif UNITY_IOS
        path = "https://warunggroup.com/vh_json/";
#else
        //Desktop (Mac OS or Windows)
        path = "file:"+ Application.dataPath + "/StreamingAssets/";
#endif
        return path + file_name;
    }

}