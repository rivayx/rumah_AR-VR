﻿using System;
using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public class GetMarketing : JSONParsing
{
    public GetMarketing(string JSONPath) : base(JSONPath) { }

    protected override List<ItemClass> JSONObjectToItems(JSONNode jsonNode)
    {
        List<ItemClass> itemClass = new List<ItemClass>();

        for (int i = 0; i < jsonNode["marketing"].Count; i++)
        {
            JSONNode node = jsonNode["marketing"][i];

            string id = node["id"];
            string name = node["name"];
            string phone = node["phone"];

            ItemClass item = new MarketingClass(id, name, phone);
            itemClass.Add(item);
        }
        return itemClass;
    }
}

public class GetType : JSONParsing
{
    public GetType(string JSONPath) : base(JSONPath) { }

    protected override List<ItemClass> JSONObjectToItems(JSONNode jsonNode)
    {
        List<ItemClass> itemClass = new List<ItemClass>();

        for (int i = 0; i < jsonNode["type"].Count; i++)
        {
            JSONNode node = jsonNode["type"][i];

            string id = node["id"];
            string name = node["name"];
            int price = node["price"].AsInt;

            ItemClass item = new TypeClass(id, name, price);
            itemClass.Add(item);
        }
        return itemClass;
    }
}