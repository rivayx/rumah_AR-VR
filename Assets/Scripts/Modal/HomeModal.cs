﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HomeModal : BaseModal
{
    [SerializeField]
    private Button _Profile;
    [SerializeField]
    private Button _AR;
    [SerializeField]
    private Button _KPR;
    [SerializeField]
    private Button _Contact;

    [SerializeField]
    private RectTransform _InfoHolder;
    [SerializeField]
    private List<Transform> _InfoItems /*= new List<Transform>()*/;

    private int infoPosition;
    private float waitTime;


    private static HomeModal _Instance;
    public static HomeModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<HomeModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no HomeModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();

        infoPosition = 0;
        waitTime = Time.fixedTime + 5;
    }


    public void IncrementInfoPosition()
    {
        if (infoPosition < _InfoHolder.childCount - 1)
        {
            infoPosition++;
            waitTime = Time.fixedTime + 5;
        }
    }
    public void DecrementInfoPosition()
    {
        if (infoPosition > 0)
        {
            infoPosition--;
            waitTime = Time.fixedTime + 5;
        }
    }

    private void InfoPosition()
    {

        if (waitTime < Time.fixedTime)
        {
            if (infoPosition < _InfoHolder.childCount - 1)
            {
                infoPosition++;
            }
            else
            {
                infoPosition = 0;
            }
            waitTime = Time.fixedTime + 5;
        }
    }


    protected override void Loop()
    {
        base.Loop();
        InfoPosition();

        float anchoredX = Mathf.Lerp(_InfoHolder.anchoredPosition.x, -520 * infoPosition, Time.deltaTime * 8);
        _InfoHolder.anchoredPosition = new Vector2(anchoredX, _InfoHolder.anchoredPosition.y);

        //float scaleSelectInfo = Mathf.Lerp(_InfoHolder.GetChild(infoPosition).localScale.x, 1, Time.deltaTime * 8);
        //_InfoHolder.GetChild(infoPosition).localScale = new Vector2(scaleSelectInfo, scaleSelectInfo);

        foreach (Transform info in _InfoItems)
        {
            if (info.GetSiblingIndex() != infoPosition)
            {
                float scaleInfo = Mathf.Lerp(info.localScale.x, .75f, Time.deltaTime * 8);
                info.localScale = new Vector2(scaleInfo, scaleInfo);
            }
            else
            {
                float scaleSelectInfo = Mathf.Lerp(info.localScale.x, 1f, Time.deltaTime * 8);
                info.localScale = new Vector2(scaleSelectInfo, scaleSelectInfo);
            }
        }

#if UNITY_ANDROID && !UNITY_IOS

        if (Input.GetKey(KeyCode.Escape))
        {
            MessageModal.Instance().OpenMessage("Apakah anda ingin keluar dari aplikasi?", Application.Quit, true);
        }

#endif
    }

    public void RegisterModal(UnityAction toProfile, UnityAction toKPR, UnityAction toAR, UnityAction toContact)
    {
        UnregisterModal();
        _Profile.onClick.AddListener(toProfile);
        _KPR.onClick.AddListener(toKPR);
        _AR.onClick.AddListener(toAR);
        _Contact.onClick.AddListener(toContact);
    }

    private void UnregisterModal()
    {
        _Profile.onClick.RemoveAllListeners();
        _KPR.onClick.RemoveAllListeners();
        _AR.onClick.RemoveAllListeners();
        _Contact.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        StopAllCoroutines();
        base.CloseModal();
    }
}
