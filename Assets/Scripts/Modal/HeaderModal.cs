﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HeaderModal : BaseModal
{
    [SerializeField]
    private Button _Back;

    private static HeaderModal _Instance;
    public static HeaderModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<HeaderModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no HeaderModal in the system");
            }
        }

        return _Instance;
    }

    public void RegisterAssetModal(UnityAction backAction)
    {
        UnRegisterAssetModal();
        OpenModal();
        if (backAction != null)
            _Back.gameObject.SetActive(true);
        else
            _Back.gameObject.SetActive(false);

        _Back.onClick.AddListener(backAction);
    }

    protected override void Loop()
    {
        base.Loop();

#if UNITY_ANDROID
        if (Input.GetKey(KeyCode.Escape))
        {
            _Back.onClick.Invoke();
        }
#endif
    }

    private void UnRegisterAssetModal()
    {
        _Back.onClick.RemoveAllListeners();
    }
}
