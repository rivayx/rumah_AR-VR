﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class VRModal : BaseModal 
{
    [SerializeField]
    private Button _Back;
    [SerializeField]
    private Text _Type;
    [SerializeField]
    private Text _Price;
    [SerializeField]
    private Button _Gyro;
    [SerializeField]
    private ControlCamera _Camera;
    [SerializeField]
    private Image _ImageGyro;
    [SerializeField]
    private Sprite _GyroMode;
    [SerializeField]
    private Sprite _TouchMode;

    private static VRModal _Instance;
    public static VRModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<VRModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no VRModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _Type.text = Singleton.Instance.Type.Name;
        _Price.text = StaticFunction.FormatCurrency(Singleton.Instance.Type.Price);
        _Gyro.gameObject.SetActive(SystemInfo.supportsGyroscope);
    }

    private void GyroAction()
    {
        if (_Camera.gyroscope)
        {
            _Camera.gyroscope = false;
        }
        else
        {
            _Camera.gyroscope = true;
        }
    }

    protected override void Loop()
    {
        base.Loop();
        _Gyro.GetComponentInChildren<Text>().text = _Camera.gyroscope ? "Touch Mode" : "Gyroscope Mode";
        _ImageGyro.sprite = _Camera.gyroscope ? _TouchMode : _GyroMode;


#if UNITY_ANDROID

        if (Input.GetKey(KeyCode.Escape))
        {
            _Back.onClick.Invoke();
        }

#endif
    }

    public void RegisterModal(UnityAction toAR)
    {
        _Back.onClick.AddListener(toAR);
        _Gyro.onClick.AddListener(GyroAction);
    }

    public void UnregisterModal()
    {
        _Back.onClick.RemoveAllListeners();
        _Gyro.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        base.CloseModal();
    }
}
