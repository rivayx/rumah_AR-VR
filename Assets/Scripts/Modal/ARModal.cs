﻿using System.Collections;
using System.Collections.Generic;
using maxstAR;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ARModal : BaseModal
{
    [SerializeField]
    private TriggerClick _Triger3D;
    [SerializeField]
    private Button _Back;
    [SerializeField]
    private Text _Type;
    [SerializeField]
    private Text _Price;
    [SerializeField]
    private Button _ToVR;
    [SerializeField]
    private Button _SpecButton;
    [SerializeField]
    private Button _ChangeType;
    [SerializeField]
    private Transform _SpecPanel;

    [SerializeField]
    private Text _Pondasi;
    [SerializeField]
    private Text _Dinding;
    [SerializeField]
    private Text _Kudakuda;
    [SerializeField]
    private Text _Atap;
    [SerializeField]
    private Text _Plafond;[
    SerializeField]
    private Text _Lantai;
    [SerializeField]
    private Text _Kusen;
    [SerializeField]
    private Text _PintuJendela;[
    SerializeField]
    private Text _Listrik;
    [SerializeField]
    private Text _Air;

    [SerializeField]
    private Transform _SpecButtonHide;
    [SerializeField]
    private Transform _SpecButtonShow;

    [SerializeField]
    private Transform _SpecPanelHide;
    [SerializeField]
    private Transform _SpecPanelShow;

    [SerializeField]
    private List<GameObject> _List3D;

    private bool showSpec;

    private static ARModal _Instance;
    public static ARModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<ARModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no ARModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        foreach(GameObject obj in _List3D)
        {
            obj.SetActive(false);
        }
        TrackerManager.GetInstance().SetTrackingOption(TrackerManager.TrackingOption.MULTI_TRACKING);
    }

    public void TypeSelected()
    {   
        foreach (GameObject obj in _List3D)
        {
            if (obj.GetComponent<TypeVariableGatherer>().id == Singleton.Instance.Type.ID)
            {
                obj.SetActive(true);
                _Type.text = Singleton.Instance.Type.Name;
                _Price.text = StaticFunction.FormatCurrency(Singleton.Instance.Type.Price);
                break;
            }
        }
    }

    public void RegisterModal(UnityAction toHome, UnityAction toVR)
    {
        UnregisterModal();
        _Back.onClick.AddListener(toHome);
        _ToVR.onClick.AddListener(toVR);
        _ChangeType.onClick.AddListener(ChangeTypeAction);
        _SpecButton.onClick.AddListener(() => showSpec = !showSpec);
        _Triger3D.TrigerAction = toVR;
    }

    private void ChangeTypeAction()
    {
        JSONGetter getJSON = JSONGetter.GetJSON();
        Debug.Log("jabdkjabsdjkbaksdbakbdkba kajbsdk kjasdb" + StaticFunction.GetFileInStreamingAssets("type.json"));
        getJSON.StartParsing(StaticFunction.GetFileInStreamingAssets("type.json"), JSONActionGetType);
    }

    public void JSONActionGetType(string jsonString)
    {
        Debug.Log(jsonString);
        PopupModal.Instance().RegisterAssetModal(TypeSelected, false);
        GetType baseJSON = new GetType(jsonString);
        PopupModal.Instance().AddItemPopup("Pilih Type Rumah", baseJSON.ParseJSON());
    }

    protected override void Loop()
    {
        base.Loop();

#if UNITY_ANDROID

        if (Input.GetKey(KeyCode.Escape))
        {
            _Back.onClick.Invoke();
        }

#endif
        if (showSpec)
        {
            _SpecPanel.position = Vector3.Lerp(_SpecPanel.position, _SpecPanelShow.position, Time.deltaTime * 8);
            _SpecButton.transform.position = Vector3.Lerp(_SpecButton.transform.position, _SpecButtonShow.position, Time.deltaTime * 8);
            _SpecButton.GetComponentInChildren<Text>().text = "Tutup Spesifikasi";
        }
        else
        {
            _SpecPanel.position = Vector3.Lerp(_SpecPanel.position, _SpecPanelHide.position, Time.deltaTime * 8);
            _SpecButton.transform.position = Vector3.Lerp(_SpecButton.transform.position, _SpecButtonHide.position, Time.deltaTime * 8);
            _SpecButton.GetComponentInChildren<Text>().text = "Lihat Spesifikasi";
        }

    }

    private void UnregisterModal()
    {
        _Back.onClick.RemoveAllListeners();
        _ToVR.onClick.RemoveAllListeners();
        _ChangeType.onClick.RemoveAllListeners();
        _SpecButton.onClick.RemoveAllListeners();
        _Triger3D.TrigerAction = null;
    }
}
