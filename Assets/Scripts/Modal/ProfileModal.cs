﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ProfileModal : BaseModal
{
    [SerializeField]
    private RawImage _MapsPanelImg;

    private string api_key = "AIzaSyAQ6S8Ydyj5IJrDC_KZzgv6TxEHIRAPtZQ";

    public float lat;
    public float lon;
    public int zoom = 16;
    public int oldzoom = 16;
    public int scale;

    private static ProfileModal _Instance;
    public static ProfileModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<ProfileModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no ProfileModal in the system");
            }
        }

        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        StartCoroutine(MapsLoader());
    }

    private IEnumerator MapsLoader()
    {
        string url = "https://maps.googleapis.com/maps/api/staticmap?center=" + lat + "," + lon +
            "&zoom=" + zoom + "&size=" + _MapsPanelImg.rectTransform.sizeDelta.x + "x" + _MapsPanelImg.rectTransform.sizeDelta.y + "&scale=" + scale +
            "&markers=color:red%7C" + lat + "," + lon + "&key=" + api_key;
        WWW www = new WWW(url);
        yield return www;

        oldzoom = zoom;
        _MapsPanelImg.texture = www.texture;
    }

    protected override void Loop()
    {
        if (zoom != oldzoom)
        {
            StartCoroutine(MapsLoader());
        }
    }

    public void RegisterModal()
    {
        UnRegisterModal();
        _MapsPanelImg.GetComponent<Button>().onClick.AddListener(() => Application.OpenURL(StaticFunction.GoogleMaps(lat, lon)));
    }

    private void UnRegisterModal()
    {
        _MapsPanelImg.GetComponent<Button>().onClick.RemoveAllListeners();
    }
}
