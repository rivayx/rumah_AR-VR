﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using SimpleJSON;

public class PopupModal : BaseModal
{
    [SerializeField]
    private Text _Title;

    [SerializeField]
    private GameObject _Item;

    [SerializeField]
    private Button _Ok;
    [SerializeField]
    private Button _Cancel;

    private UnityAction OkAction;

    private GameObject ObjectSelected;
    private List<Toggle> ToggleList = new List<Toggle>();

    private static PopupModal _Instance;
    public static PopupModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<PopupModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no PopupModal in the system");
            }
        }
        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _Item.SetActive(false);
        _Ok.interactable = false;
        DestroyListGameObject();
    }

    public void AddItemPopup(string title, List<ItemClass> listType)
    {
        OpenModal();
        _Title.text = title;

        foreach (ItemClass itemClass in listType)
        {
            TypeClass type = itemClass as TypeClass;
            GameObject item = Instantiate(_Item, _Item.transform.parent);
            item.name = type.Name;
            item.SetActive(true);

            TypeVariableGatherer itemVar = item.GetComponent<TypeVariableGatherer>();
            itemVar.id = type.ID;
            itemVar.Label.text = type.Name;
            itemVar.price = type.Price;

            ToggleList.Add(item.GetComponent<Toggle>());
        }
        if (ToggleList.Count == 1)
        {
            ToggleList[0].isOn = true;
            _Ok.onClick.Invoke();
        }
    }

    public void RegisterAssetModal(UnityAction okAction, bool viewCancel)
    {
        UnRegisterAssetModal();
        OkAction = okAction;
        _Ok.onClick.AddListener(GetSelectType);
        _Cancel.onClick.AddListener(CloseModal);
        _Cancel.gameObject.SetActive(viewCancel);
    }

    private void GetSelectType()
    {
        foreach (Toggle toggle in ToggleList)
        {
            if (toggle.isOn)
            {
                TypeVariableGatherer typeVar = toggle.GetComponent<TypeVariableGatherer>();
                Singleton.Instance.Type = new TypeClass(typeVar.id, typeVar.Label.text, typeVar.price);
                break;
            }
        }
        CloseModal();

        if (OkAction != null)
            OkAction();
    }

    protected override void Loop()
    {
        base.Loop();
        if (ToggleList.Count > 0)
        {
            foreach (Toggle toggle in ToggleList)
            {
                if (toggle.isOn)
                {
                    _Ok.interactable = true;
                    break;
                }
            }
        }
    }

    private void DestroyListGameObject()
    {
        if (ToggleList.Count > 0)
        {
            foreach (Toggle toggle in ToggleList)
            {
                toggle.isOn = false;
                Destroy(toggle.gameObject);
            }
            ToggleList.Clear();
        }
    }

    public void UnRegisterAssetModal()
    {
        _Ok.onClick.RemoveAllListeners();
        _Cancel.onClick.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        DestroyListGameObject();

        base.CloseModal();
    }
}   