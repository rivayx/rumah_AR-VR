﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class KPRModal : BaseModal
{
    //[SerializeField]
    //private Dropdown _Type;
    [SerializeField]
    private InputField _Harga;
    [SerializeField]
    private InputField _DPPersen;
    [SerializeField]
    private InputField _DP;
    [SerializeField]
    private InputField _Tenor;
    [SerializeField]
    private InputField _Margin;

    //[SerializeField]
    //private InputField _JumlahPinjaman;
    [SerializeField]
    private InputField _Angsuran;

    private string harga = "0";
    private string dp_persen = "0";
    private string dp = "0";
    private string tenor = "0";
    private string margin = "0";

    private string angsuran = "";

    private bool perhitunganIsDone;

    private sel currentselect;

    private enum sel
    {
        Null,
        harga,
        dp_persen,
        dp,
    }
    private static KPRModal _Instance;
    public static KPRModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<KPRModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no KPRModal in the system");
            }
        }

        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        currentselect = sel.Null;
    }

    public void RegisterModal()
    {
        UnRegisterModal();
        _Harga.onEndEdit.AddListener(EndEdit);
        _DPPersen.onEndEdit.AddListener(EndEdit);
        _DP.onEndEdit.AddListener(EndEdit);
        _Tenor.onEndEdit.AddListener(EndEdit);
        _Margin.onEndEdit.AddListener(EndEdit);
    }

    public void SetSelectUI(Selectable selected)
    {
        selected.enabled = true;

        if (selected == _Harga)
        {
            currentselect = sel.harga;
            _Harga.text = harga;
        }
        if (selected == _DP)
        {
            currentselect = sel.dp;
            _DP.text = dp;
        }
        if (selected == _DPPersen)
        {
            currentselect = sel.dp_persen;
            _DPPersen.text = dp_persen;
        }

        Singleton.Instance.GetEventSystem.SetSelectedGameObject(selected.gameObject);
    }

    public void EndEdit(string val)
    {

        if (currentselect == sel.harga)
        {
            harga = _Harga.text;
            _Harga.enabled = false;

            _Harga.text = StaticFunction.FormatGeneral(double.Parse(harga));

            dp = "0";
            dp_persen = "0";

            _DP.text = "";
            _DPPersen.text = "";

        }
        if (currentselect == sel.dp)
        {
            dp = _DP.text;
            _DP.enabled = false;

            dp_persen = (float.Parse(dp) / float.Parse(harga) * 100).ToString("F2");

            _DP.text = StaticFunction.FormatGeneral(double.Parse(dp));
            _DPPersen.text = dp_persen;
        }
        if (currentselect == sel.dp_persen)
        {
            dp_persen = _DPPersen.text;
            _DPPersen.enabled = false;

            dp = (float.Parse(dp_persen) / 100 * float.Parse(harga)).ToString();
            _DPPersen.text = dp_persen;
            _DP.text = StaticFunction.FormatGeneral(double.Parse(dp));
            dp = _DP.text.Replace(".", "");
        } 

        tenor = _Tenor.text;
        _Tenor.enabled = false;

        margin = _Margin.text;
        _Margin.enabled = false;

        currentselect = sel.Null;
        perhitunganIsDone = false;
    }

    protected override void Loop()
    {
        base.Loop();

        if (_Harga.text != "" && _DP.text != "" && _Tenor.text != "" && _Margin.text != "")
        {
            if (!perhitunganIsDone)
                Perhitungan();
        }
    }

    private void Perhitungan()
    {
        try
        {
            float fharga = float.Parse(harga);
            float fdp = float.Parse(dp);
            float ftenor = float.Parse(tenor);
            float fmargin = float.Parse(margin) / 100 / 12;
            float jumlah_bulan = ftenor * 12;
            float pokok_kredit = fharga - fdp;
            float fangsuran = 0f;

            fangsuran = fmargin * -pokok_kredit * (float)Math.Pow((1 + fmargin), jumlah_bulan) / (1 - (float)Math.Pow((1 + fmargin), jumlah_bulan));
            angsuran = fangsuran.ToString("F");

            _Angsuran.text = StaticFunction.FormatGeneral(double.Parse(angsuran));

            perhitunganIsDone = true;
        }
        catch
        {
            //Debug.Log("Belum Kelar");
        }
    }

    private void UnRegisterModal()
    {
        _Harga.onEndEdit.RemoveAllListeners();
        _DPPersen.onEndEdit.RemoveAllListeners();
        _DP.onEndEdit.RemoveAllListeners();
        _Tenor.onEndEdit.RemoveAllListeners();
        _Margin.onEndEdit.RemoveAllListeners();
    }

    public override void CloseModal()
    {
        harga = "0";
        dp_persen = "0";
        dp = "0";
        tenor = "0";
        margin = "0";
        angsuran = "";

        _Harga.text = "";
        _DPPersen.text = "";
        _DP.text = "";
        _Tenor.text = "";
        _Margin.text = "";
        _Angsuran.text = "";

        base.CloseModal();
    }
}
