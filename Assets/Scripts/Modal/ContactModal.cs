﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class ContactModal : BaseModal
{
    [SerializeField]
    private Button _MarketingOffice;
    [SerializeField]
    private GameObject _MarketingItem;

    private List<GameObject> ItemMarketingList = new List<GameObject>();

    private List<ItemClass> MarketingList;

    private static ContactModal _Instance;
    public static ContactModal Instance()
    {
        if (_Instance == null)
        {
            _Instance = FindObjectOfType<ContactModal>();

            if (_Instance == null)
            {
                Debug.LogError("there is no ContactModal in the system");
            }
        }

        return _Instance;
    }

    public override void OpenModal()
    {
        base.OpenModal();
        _MarketingItem.SetActive(false);

        JSONGetter getJSON = JSONGetter.GetJSON();
        Debug.Log(StaticFunction.GetFileInStreamingAssets("marketing.json"));
        getJSON.StartParsing(StaticFunction.GetFileInStreamingAssets("marketing.json"), jsonActionGetData);

    }

    private void jsonActionGetData(string jsonString)
    {
        Debug.Log(jsonString);
        GetMarketing baseJSON = new GetMarketing(jsonString);
        MarketingList = baseJSON.ParseJSON();
        StartCoroutine(CreateItem());
    }

    private IEnumerator CreateItem()
    {
        foreach (ItemClass itemClass in MarketingList)
        {
            yield return new WaitForEndOfFrame();
            MarketingClass marketingItemClass = itemClass as MarketingClass;
            GameObject item = Instantiate(_MarketingItem, _MarketingItem.transform.parent);
            MarketingVariableGatherer itemVar = item.GetComponent<MarketingVariableGatherer>();

            item.SetActive(true);
            item.name = marketingItemClass.Name;
            itemVar.Name.text = marketingItemClass.Name;
            itemVar.ID = marketingItemClass.ID;
            itemVar.Phone = marketingItemClass.Phone;

            item.GetComponent<Button>().onClick.AddListener(() => Application.OpenURL(StaticFunction.Whatsapp(itemVar.Phone)));
            ItemMarketingList.Add(item);
        }
    }

    public void RegisterModal()
    {
        UnRegisterModal();
        _MarketingOffice.onClick.AddListener(() => Application.OpenURL("tel:+6285777722489"));
    }

    private void UnRegisterModal()
    {
        _MarketingOffice.onClick.RemoveAllListeners();
    }
}
