﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class ARState : FSMState
{
    private AsyncOperation _LoadAR;
    private bool bSceneLoaded;

    public ARState()
    {
        stateID = StateID.AR;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter ARState");
        bSceneLoaded = false;

        if (SceneManager.GetActiveScene().name != "AR")
            _LoadAR = SceneManager.LoadSceneAsync("AR");
        else
            EnterARState();

        base.OnEnter();
    }

    private void EnterARState()
    {
        ARModal arModal = ARModal.Instance();
        arModal.OpenModal();
        arModal.RegisterModal(ToHome, ToVR);
        bSceneLoaded = true;

        if (Singleton.Instance.Type == null)
        {
            JSONGetter getJSON = JSONGetter.GetJSON();
            getJSON.StartParsing(StaticFunction.GetFileInStreamingAssets("type.json"), arModal.JSONActionGetType);
        }

        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    private void ToHome()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    private void ToVR()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_VR);
    }

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadAR.isDone)
            {
                EnterARState();
            }
            else
            {
                MessageModal.Instance().Loading(_LoadAR.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave ARState");
        ARModal arModal = ARModal.Instance();
        arModal.CloseModal();

        base.OnLeave();
    }
}
