﻿using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class HomeState : FSMState
{
    private AsyncOperation _LoadHome;
    private bool bSceneLoaded;

    public HomeState()
    {
        stateID = StateID.HOME;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter HomeState");
        bSceneLoaded = false;

        if (SceneManager.GetActiveScene().name != "UI")
            _LoadHome = SceneManager.LoadSceneAsync("UI");
        else
            EnterHomeState();
        
        base.OnEnter();
    }

    private void EnterHomeState()
    {
        HomeModal modal = HomeModal.Instance();
        modal.OpenModal();
        modal.RegisterModal(ToProfile, ToKPR, ToAR, ToContact);
        HeaderModal.Instance().RegisterAssetModal(null);
        bSceneLoaded = true;

        Singleton.Instance.Type = null;

        Screen.orientation = ScreenOrientation.Portrait;
    }

    private void ToProfile()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_PROFILE);
    }

    private void ToAR()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_AR);
    }

    private void ToKPR()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_KPR);
    }

    private void ToContact()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_CONTACT);
    }

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadHome.isDone)
            {
                EnterHomeState();
            }
            else
            {
                MessageModal.Instance().Loading(_LoadHome.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave HomeState");
        
        HomeModal modal = HomeModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}