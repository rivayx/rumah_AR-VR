﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.SceneManagement;

public class VRState : FSMState
{
    private AsyncOperation _LoadVR;
    private bool bSceneLoaded;

    public VRState()
    {
        stateID = StateID.VR;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter VRState");
        bSceneLoaded = false;

        if (SceneManager.GetActiveScene().name != "VR")
            _LoadVR = SceneManager.LoadSceneAsync("VR");
        else
            EnterVRState();

        base.OnEnter();
    }

    private void EnterVRState()
    {
        VRModal vrModal = VRModal.Instance();
        vrModal.OpenModal();
        vrModal.RegisterModal(ToAR);
        bSceneLoaded = true;

        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    private void ToAR()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_AR);
    }

    public override void Update()
    {
        if (!bSceneLoaded)
        {
            if (_LoadVR.isDone)
            {
                EnterVRState();
            }
            else
            {
                MessageModal.Instance().Loading(_LoadVR.progress);
            }
        }
    }

    public override void OnLeave()
    {
        Debug.Log("Leave VRState");
        VRModal vrModal = VRModal.Instance();
        vrModal.UnregisterModal();
        vrModal.CloseModal();

        base.OnLeave();
    }
}
