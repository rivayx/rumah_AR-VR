﻿using BaseApps;
using UnityEngine;

public class KPRState : FSMState
{
    public KPRState()
    {
        stateID = StateID.KPR;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter KPRState");

        KPRModal modal = KPRModal.Instance();
        modal.OpenModal();
        modal.RegisterModal();

        HeaderModal.Instance().RegisterAssetModal(ToHome);

        base.OnEnter();
    }

    private void ToHome()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    public override void OnLeave()
    {
        Debug.Log("Leave KPRState");
        KPRModal modal = KPRModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}
