﻿using System.Collections;
using System.Collections.Generic;
using BaseApps;
using UnityEngine;

public class ContactState : FSMState
{

    public ContactState()
    {
        stateID = StateID.CONTACT;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter ContactState");

        ContactModal modal = ContactModal.Instance();
        modal.OpenModal();
        modal.RegisterModal();

        HeaderModal.Instance().RegisterAssetModal(ToHome);

        base.OnEnter();
    }

    private void ToHome()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    public override void OnLeave()
    {
        Debug.Log("Leave ContactState");
        ContactModal modal = ContactModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}
