﻿using UnityEngine;
using BaseApps;

public class ProfileState : FSMState
{
    public ProfileState()
    {
        stateID = StateID.PROFILE;
    }

    public override void OnEnter()
    {
        Debug.Log("Enter ProfileState");

        ProfileModal modal = ProfileModal.Instance();
        modal.OpenModal();
        modal.RegisterModal();

        HeaderModal.Instance().RegisterAssetModal(ToHome);

        base.OnEnter();
    }

    private void ToHome()
    {
        BaseRuntime appRuntime = _FSMCaller as BaseRuntime;
        appRuntime.SetTransition(Transition.TRANSITION_TO_HOME);
    }

    public override void OnLeave()
    {
        Debug.Log("Leave ProfileState");
        ProfileModal modal = ProfileModal.Instance();
        modal.CloseModal();

        base.OnLeave();
    }
}
