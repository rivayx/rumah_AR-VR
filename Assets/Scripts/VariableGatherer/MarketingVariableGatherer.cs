﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketingVariableGatherer : BaseVariableGatherer
{
    public Text Name;
    public string ID { get; set; }
    public string Phone { get; set; }
}
