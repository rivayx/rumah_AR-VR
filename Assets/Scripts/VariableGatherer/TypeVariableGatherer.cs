﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TypeVariableGatherer : BaseVariableGatherer
{
    public Text Label;
    public string id;
    public int price { get; set; }
}
